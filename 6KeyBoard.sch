EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW1
U 1 1 5ED51EBA
P 5200 3500
F 0 "SW1" H 5200 3785 50  0000 C CNN
F 1 "SW_Push" H 5200 3694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 5200 3700 50  0001 C CNN
F 3 "~" H 5200 3700 50  0001 C CNN
	1    5200 3500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5ED52DC0
P 6200 4500
F 0 "SW4" H 6200 4785 50  0000 C CNN
F 1 "SW_Push" H 6200 4694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 6200 4700 50  0001 C CNN
F 3 "~" H 6200 4700 50  0001 C CNN
	1    6200 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5ED534EF
P 7200 4500
F 0 "SW7" H 7200 4785 50  0000 C CNN
F 1 "SW_Push" H 7200 4694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 7200 4700 50  0001 C CNN
F 3 "~" H 7200 4700 50  0001 C CNN
	1    7200 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5ED544A5
P 5200 4000
F 0 "SW2" H 5200 4285 50  0000 C CNN
F 1 "SW_Push" H 5200 4194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 5200 4200 50  0001 C CNN
F 3 "~" H 5200 4200 50  0001 C CNN
	1    5200 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5ED54F55
P 6200 4000
F 0 "SW5" H 6200 4285 50  0000 C CNN
F 1 "SW_Push" H 6200 4194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 6200 4200 50  0001 C CNN
F 3 "~" H 6200 4200 50  0001 C CNN
	1    6200 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5ED5560C
P 7200 4000
F 0 "SW8" H 7200 4285 50  0000 C CNN
F 1 "SW_Push" H 7200 4194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 7200 4200 50  0001 C CNN
F 3 "~" H 7200 4200 50  0001 C CNN
	1    7200 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 5ED5626A
P 8200 4500
F 0 "SW10" H 8200 4785 50  0000 C CNN
F 1 "SW_Push" H 8200 4694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 8200 4700 50  0001 C CNN
F 3 "~" H 8200 4700 50  0001 C CNN
	1    8200 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5ED56999
P 5200 4500
F 0 "SW3" H 5200 4785 50  0000 C CNN
F 1 "SW_Push" H 5200 4694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 5200 4700 50  0001 C CNN
F 3 "~" H 5200 4700 50  0001 C CNN
	1    5200 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5ED57575
P 6200 3500
F 0 "SW6" H 6200 3785 50  0000 C CNN
F 1 "SW_Push" H 6200 3694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 6200 3700 50  0001 C CNN
F 3 "~" H 6200 3700 50  0001 C CNN
	1    6200 3500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 5ED57BA7
P 7200 3500
F 0 "SW9" H 7200 3785 50  0000 C CNN
F 1 "SW_Push" H 7200 3694 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 7200 3700 50  0001 C CNN
F 3 "~" H 7200 3700 50  0001 C CNN
	1    7200 3500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 5ED5807C
P 8200 4000
F 0 "SW11" H 8200 4285 50  0000 C CNN
F 1 "SW_Push" H 8200 4194 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate_SmallerVias" H 8200 4200 50  0001 C CNN
F 3 "~" H 8200 4200 50  0001 C CNN
	1    8200 4000
	1    0    0    -1  
$EndComp
$Comp
L MCP23008:MCP23008-E_SO U1
U 1 1 5ED570A6
P 2850 3950
F 0 "U1" H 2850 5120 50  0000 C CNN
F 1 "MCP23008-E_SO" H 2850 5029 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-18W_7.5x11.6mm_Pitch1.27mm" H 2850 3950 50  0001 L BNN
F 3 "" H 2850 3950 50  0001 C CNN
	1    2850 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5ED58498
P 1950 3000
F 0 "R1" H 2020 3046 50  0000 L CNN
F 1 "1K" H 2020 2955 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1880 3000 50  0001 C CNN
F 3 "~" H 1950 3000 50  0001 C CNN
	1    1950 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3350 2000 3350
Wire Wire Line
	2000 3350 2000 3450
Wire Wire Line
	2000 3450 2150 3450
Connection ~ 2000 3450
Wire Wire Line
	2150 3650 1750 3650
Wire Wire Line
	2150 3250 2000 3250
Wire Wire Line
	1950 3250 1950 3150
Wire Wire Line
	1950 2850 1950 2700
Wire Wire Line
	1950 2700 2050 2700
Text Label 1950 2700 0    50   ~ 0
VDD
Wire Wire Line
	2000 3350 2000 3250
Connection ~ 2000 3350
Connection ~ 2000 3250
Wire Wire Line
	2000 3250 1950 3250
Wire Wire Line
	2150 3850 2000 3850
Text Label 1800 3650 0    50   ~ 0
SCL
Wire Wire Line
	2150 4050 1700 4050
Wire Wire Line
	2150 4150 1700 4150
Wire Wire Line
	1700 4250 2150 4250
Wire Wire Line
	2150 4350 1700 4350
Wire Wire Line
	2150 4550 1700 4550
Wire Wire Line
	3550 3050 3850 3050
Wire Wire Line
	3550 4050 4000 4050
Wire Wire Line
	3550 4150 4000 4150
Wire Wire Line
	3550 4250 4000 4250
Wire Wire Line
	3550 4650 4000 4650
$Comp
L power:GND #PWR0101
U 1 1 5ED5D5DB
P 4000 4800
F 0 "#PWR0101" H 4000 4550 50  0001 C CNN
F 1 "GND" H 4005 4627 50  0000 C CNN
F 2 "" H 4000 4800 50  0001 C CNN
F 3 "" H 4000 4800 50  0001 C CNN
	1    4000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4800 4000 4650
Wire Wire Line
	5000 3500 4900 3500
Wire Wire Line
	5400 3500 5450 3500
Wire Wire Line
	5450 3500 5450 3600
Wire Wire Line
	5400 4000 5450 4000
Wire Wire Line
	5450 4000 5450 4100
Wire Wire Line
	5000 4500 4900 4500
Wire Wire Line
	5400 4500 5450 4500
Wire Wire Line
	5450 4500 5450 4600
Wire Wire Line
	6400 3500 6450 3500
Wire Wire Line
	6450 3500 6450 3600
Wire Wire Line
	6400 4000 6450 4000
Wire Wire Line
	6450 4000 6450 4100
Wire Wire Line
	6400 4500 6450 4500
Wire Wire Line
	6450 4500 6450 4600
Wire Wire Line
	7400 3500 7450 3500
Wire Wire Line
	7450 3500 7450 3600
Wire Wire Line
	7400 4000 7450 4000
Wire Wire Line
	7450 4000 7450 4100
Wire Wire Line
	7400 4500 7450 4500
Wire Wire Line
	7450 4500 7450 4600
Wire Wire Line
	8400 4500 8450 4500
Wire Wire Line
	8450 4500 8450 4600
Wire Wire Line
	8400 4000 8450 4000
Wire Wire Line
	8450 4000 8450 4100
Wire Wire Line
	4900 3500 4900 4000
Wire Wire Line
	4900 4000 5000 4000
Wire Wire Line
	4900 4000 4900 4500
Connection ~ 4900 4000
Wire Wire Line
	4900 4500 4900 4750
Wire Wire Line
	4900 4750 5000 4750
Connection ~ 4900 4500
Wire Wire Line
	5800 3500 5800 4000
Wire Wire Line
	5800 4000 5800 4500
Connection ~ 5800 4000
Wire Wire Line
	5800 4500 5800 4750
Wire Wire Line
	5800 4750 5900 4750
Connection ~ 5800 4500
Wire Wire Line
	5800 3500 6000 3500
Wire Wire Line
	5800 4000 6000 4000
Wire Wire Line
	5800 4500 6000 4500
Wire Wire Line
	6800 3500 6800 4000
Wire Wire Line
	6800 4000 6800 4500
Connection ~ 6800 4000
Wire Wire Line
	6800 4500 6800 4750
Wire Wire Line
	6800 4750 6900 4750
Connection ~ 6800 4500
Wire Wire Line
	6800 3500 7000 3500
Wire Wire Line
	6800 4000 7000 4000
Wire Wire Line
	6800 4500 7000 4500
Wire Wire Line
	7800 4000 7800 4500
Wire Wire Line
	7800 4500 7800 4750
Wire Wire Line
	7800 4750 7900 4750
Connection ~ 7800 4500
Wire Wire Line
	7800 4500 8000 4500
Wire Wire Line
	7800 4000 8000 4000
Wire Wire Line
	7450 3600 6450 3600
Wire Wire Line
	6450 3600 5450 3600
Connection ~ 6450 3600
Wire Wire Line
	5450 3600 4700 3600
Connection ~ 5450 3600
Wire Wire Line
	5450 4100 4700 4100
Wire Wire Line
	5450 4600 4700 4600
Wire Wire Line
	8450 4100 7450 4100
Wire Wire Line
	7450 4100 6450 4100
Connection ~ 7450 4100
Wire Wire Line
	5450 4100 6450 4100
Connection ~ 5450 4100
Connection ~ 6450 4100
Wire Wire Line
	5450 4600 6450 4600
Connection ~ 5450 4600
Wire Wire Line
	6450 4600 7450 4600
Connection ~ 6450 4600
Wire Wire Line
	7450 4600 8450 4600
Connection ~ 7450 4600
Text Label 4750 3600 0    50   ~ 0
Row0
Text Label 4750 4600 0    50   ~ 0
Row2
Text Label 4750 4100 0    50   ~ 0
Row1
Text Label 4950 4750 0    50   ~ 0
Col0
Text Label 5850 4750 0    50   ~ 0
Col1
Text Label 6850 4750 0    50   ~ 0
Col2
Text Label 7850 4750 0    50   ~ 0
Col3
Text Label 1900 4250 0    50   ~ 0
Col0
Text Label 1900 4150 0    50   ~ 0
Col1
Text Label 1900 4350 0    50   ~ 0
Col2
Text Label 1900 4050 0    50   ~ 0
Col3
Text Label 3700 4050 0    50   ~ 0
Row0
Text Label 3700 4150 0    50   ~ 0
Row1
Text Label 3700 4250 0    50   ~ 0
Row2
NoConn ~ 3550 3250
Text Label 3700 3050 0    50   ~ 0
VDD
Text Label 1900 4550 0    50   ~ 0
SDA
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5ED8BDB4
P 2550 1550
F 0 "J1" H 2630 1542 50  0000 L CNN
F 1 "Conn_01x04" H 2630 1451 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 2550 1550 50  0001 C CNN
F 3 "~" H 2550 1550 50  0001 C CNN
	1    2550 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 1550 1950 1550
Wire Wire Line
	2350 1650 1950 1650
Wire Wire Line
	2350 1750 1950 1750
Wire Wire Line
	1650 1450 1650 1550
Wire Wire Line
	1650 1450 2350 1450
$Comp
L power:GND #PWR0102
U 1 1 5ED97634
P 1650 1550
F 0 "#PWR0102" H 1650 1300 50  0001 C CNN
F 1 "GND" H 1655 1377 50  0000 C CNN
F 2 "" H 1650 1550 50  0001 C CNN
F 3 "" H 1650 1550 50  0001 C CNN
	1    1650 1550
	1    0    0    -1  
$EndComp
Text Label 2100 1750 0    50   ~ 0
SDA
Text Label 2100 1650 0    50   ~ 0
SCL
Text Label 2100 1550 0    50   ~ 0
VDD
$Comp
L Device:C C1
U 1 1 5ED9891D
P 3850 3250
F 0 "C1" H 3965 3296 50  0000 L CNN
F 1 "1uF" H 3965 3205 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 3888 3100 50  0001 C CNN
F 3 "~" H 3850 3250 50  0001 C CNN
	1    3850 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5ED9A2C1
P 3850 3450
F 0 "#PWR0103" H 3850 3200 50  0001 C CNN
F 1 "GND" H 3855 3277 50  0000 C CNN
F 2 "" H 3850 3450 50  0001 C CNN
F 3 "" H 3850 3450 50  0001 C CNN
	1    3850 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3450 3850 3400
Wire Wire Line
	3850 3100 3850 3050
Connection ~ 3850 3050
Wire Wire Line
	3850 3050 4050 3050
Text Notes 5100 3000 0    50   ~ 0
NOTE: Schematic position does NOT match layout position.
$Comp
L Mechanical:MountingHole H1
U 1 1 5EDDD559
P 4500 1500
F 0 "H1" H 4600 1546 50  0000 L CNN
F 1 "MountingHole" H 4600 1455 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 4500 1500 50  0001 C CNN
F 3 "~" H 4500 1500 50  0001 C CNN
	1    4500 1500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5EDDE116
P 5000 1500
F 0 "H2" H 5100 1546 50  0000 L CNN
F 1 "MountingHole" H 5100 1455 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 5000 1500 50  0001 C CNN
F 3 "~" H 5000 1500 50  0001 C CNN
	1    5000 1500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5EDDE7CF
P 6000 1500
F 0 "H4" H 6100 1546 50  0000 L CNN
F 1 "MountingHole" H 6100 1455 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 6000 1500 50  0001 C CNN
F 3 "~" H 6000 1500 50  0001 C CNN
	1    6000 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3450 2000 3850
NoConn ~ 2150 3550
$Comp
L Device:R R3
U 1 1 5ED8046B
P 1650 3000
F 0 "R3" H 1720 3046 50  0000 L CNN
F 1 "1K" H 1720 2955 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1580 3000 50  0001 C CNN
F 3 "~" H 1650 3000 50  0001 C CNN
	1    1650 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5ED807FC
P 1400 3000
F 0 "R2" H 1470 3046 50  0000 L CNN
F 1 "1K" H 1470 2955 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1330 3000 50  0001 C CNN
F 3 "~" H 1400 3000 50  0001 C CNN
	1    1400 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 2850 1400 2700
Wire Wire Line
	1400 2700 1650 2700
Connection ~ 1950 2700
Wire Wire Line
	1650 2850 1650 2700
Connection ~ 1650 2700
Wire Wire Line
	1650 2700 1950 2700
Wire Wire Line
	1400 3150 1050 3150
Wire Wire Line
	1650 3150 1650 3250
Wire Wire Line
	1650 3250 1050 3250
Text Label 1150 3250 0    50   ~ 0
SDA
Text Label 1150 3150 0    50   ~ 0
SCL
Wire Wire Line
	3550 4350 4350 4350
$Comp
L Device:LED D1
U 1 1 5ED851D5
P 4350 4150
F 0 "D1" V 4389 4033 50  0000 R CNN
F 1 "LED" V 4298 4033 50  0000 R CNN
F 2 "LEDs:LED_0603_HandSoldering" H 4350 4150 50  0001 C CNN
F 3 "~" H 4350 4150 50  0001 C CNN
	1    4350 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4350 4300 4350 4350
Text Label 4450 3550 0    50   ~ 0
VDD
$Comp
L Device:R R4
U 1 1 5ED8CA9D
P 4350 3800
F 0 "R4" H 4420 3846 50  0000 L CNN
F 1 "1K" H 4420 3755 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4280 3800 50  0001 C CNN
F 3 "~" H 4350 3800 50  0001 C CNN
	1    4350 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4000 4350 3950
Wire Wire Line
	4350 3650 4350 3550
Wire Wire Line
	4350 3550 4450 3550
$EndSCHEMATC
